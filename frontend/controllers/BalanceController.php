<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Description of BalanceController
 *
 * @author User
 */
class BalanceController  extends Controller{
    
    public $layout = "custom_layout";
    
    public function actionIndex()
    {
        $birja = new \frontend\models\Binance(BINANCE_API_KEY,BINANCE_API_SECURE);
        return $this->render('index', ['balance' => $birja->getBalance()]);
    }
}
