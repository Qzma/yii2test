<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

/**
 * Description of Binance
 *
 * @author User
 */
class Binance extends Birja{
    
    private $_api;
    
    public function __construct($key, $secure) {
        $this->_api  = new \Binance\API($key,$secure);
    }
    
    public function getBalance() {
        $this->_api->useServerTime();
        
        $ticker = $this->_api->prices();
        $balance = $this->_api->balances($ticker);
        
        if(TESTMODE){
            //У меня нет крипты поэтому просто мокнем этот массив
            $balance = [
                "WTC" => [
                    "available" => 909.61000000,
                    "onOrder" => 0.00000000,
                    "btcValue" => 0.94015470
                ],
                "BNB" =>[
                    "available" => 1045.94316876,
                    "onOrder" => 0.00000000,
                    "btcValue" => 0.21637426
                ]
            ];
        }
        return $balance;
    }
}
